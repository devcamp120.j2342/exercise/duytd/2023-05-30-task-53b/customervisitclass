import models.Customer;
import models.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Kari");
        System.out.println("Customer 1");
        System.out.println(customer1);

        Customer customer2 = new Customer("Mike");
        System.out.println("Customer 2");
        System.out.println(customer2);

        Visit visit1 = new Visit(customer1, null);
        System.out.println("Visit 1");
        System.out.println(visit1);
        System.out.println("Tong chi phi");
        System.out.println(visit1.getTotalExpence());

        Visit visit2 = new Visit(customer2, null);
        System.out.println("Visit 2");
        System.out.println(visit2);
        System.out.println("Tong chi phi");
        System.out.println(visit2.getTotalExpence());

    }
}
